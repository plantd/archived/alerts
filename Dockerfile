FROM openjdk:8

COPY target/alerts-0.0.1-SNAPSHOT.jar /usr/bin/plantd-alerts

CMD ["java", "-jar", "/usr/bin/plantd-alerts"]
