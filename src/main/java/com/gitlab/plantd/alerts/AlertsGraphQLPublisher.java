package com.gitlab.plantd.alerts;

import com.gitlab.plantd.alerts.publishers.AlertPublisher;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;

@Component
public class AlertsGraphQLPublisher {
    private final static AlertPublisher ALERT_PUBLISHER = new AlertPublisher();

    private final GraphQLSchema graphQLSchema;

    public AlertsGraphQLPublisher() {
        graphQLSchema = buildSchema();
    }

    private GraphQLSchema buildSchema() {
        //
        // reads a file that provides the schema types
        //
        Reader streamReader = loadSchemaFile("schema.graphqls");
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(streamReader);

        RuntimeWiring wiring = RuntimeWiring.newRuntimeWiring()
            .type(newTypeWiring("Subscription")
                .dataFetcher("alerts", alertsSubscriptionFetcher())
            )
            .build();

        return new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
    }

    private DataFetcher alertsSubscriptionFetcher() {
        return environment -> {
            int level = environment.getArgument("level");
            return ALERT_PUBLISHER.getPublisher(level);
        };
    }

    public GraphQLSchema getGraphQLSchema() {
        return graphQLSchema;
    }

    @SuppressWarnings("SameParameterValue")
    private Reader loadSchemaFile(String name) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(name);
        return new InputStreamReader(stream);
    }
}
