package com.gitlab.plantd.alerts.publishers;

import com.gitlab.plantd.alerts.resolvers.Alert;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class AlertPublisher {

    private static final Logger LOG = LoggerFactory.getLogger(AlertPublisher.class);

    private final Flowable<Alert> publisher;

    public AlertPublisher() {
        Observable<Alert> alertUpdateObservable = Observable.create(emitter -> {
            ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
            executorService.scheduleAtFixedRate(newAlert(emitter), 0, 2, TimeUnit.SECONDS);
        });

        ConnectableObservable<Alert> connectableObservable = alertUpdateObservable.share().publish();
        connectableObservable.connect();

        publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
    }

    private Runnable newAlert(ObservableEmitter<Alert> emitter) {
        return () -> {
            List<Alert> alerts = getUpdates(rollDice(0, 5));
            if (alerts != null) {
                emitAlerts(emitter, alerts);
            }
        };
    }

    private void emitAlerts(ObservableEmitter<Alert> emitter, List<Alert> alerts) {
        for (Alert alert : alerts) {
            try {
                emitter.onNext(alert);
            } catch (RuntimeException e) {
                LOG.error("Cannot send alert", e);
            }
        }
    }

    public Flowable<Alert> getPublisher() {
        return publisher;
    }

    public Flowable<Alert> getPublisher(int level) {
        if (level >= 0 && level <= 5) {
            return publisher.filter(alert -> level == alert.getLevel());
        }
        return publisher;
    }

    private List<Alert> getUpdates(int number) {
        List<Alert> updates = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            updates.add(rollUpdate());
        }
        return updates;
    }

    // Mock data

    private final static Map<String, Alert> ALERTS = new ConcurrentHashMap<>();

    static {
        ALERTS.put("alert-1", new Alert("alert-1", 0, "service-1", "message"));
        ALERTS.put("alert-2", new Alert("alert-2", 1, "service-1", "info"));
        ALERTS.put("alert-3", new Alert("alert-3", 2, "service-1", "error"));
        ALERTS.put("alert-4", new Alert("alert-4", 3, "service-2", "warning"));
        ALERTS.put("alert-5", new Alert("alert-5", 4, "service-2", "debug"));
    }

    private Alert rollUpdate() {
        ArrayList<String> ALERT_IDS = new ArrayList<>(ALERTS.keySet());
        String alertId = ALERT_IDS.get(rollDice(0, ALERT_IDS.size() - 1));
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        Alert alert = ALERTS.get(alertId);

        ALERTS.put(alertId, alert);
        alert.setMessage(LocalDateTime.now().format(formatter));

        return alert;
    }

    private final static Random rand = new Random();

    private static int rollDice(int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }
}
