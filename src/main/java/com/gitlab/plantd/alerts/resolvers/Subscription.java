package com.gitlab.plantd.alerts.resolvers;

import com.gitlab.plantd.alerts.publishers.AlertPublisher;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;

@Component
public class Subscription {

    private AlertPublisher alertPublisher;

    Subscription(AlertPublisher alertPublisher) {
        this.alertPublisher = alertPublisher;
    }

    Publisher<Alert> alerts(int level) {
        return alertPublisher.getPublisher(level);
    }
}
