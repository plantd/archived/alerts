package com.gitlab.plantd.alerts.resolvers;

public class Alert {

    private String id;
    private int level;
    private String serviceName;
    private String message;

    public Alert(String id, int level, String serviceName, String message) {
        this.id = id;
        this.level = level;
        this.serviceName = serviceName;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
