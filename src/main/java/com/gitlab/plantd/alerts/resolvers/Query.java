package com.gitlab.plantd.alerts.resolvers;

import com.google.common.collect.ImmutableMap;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class Query {

    private static List<Map<String, String>> alerts = Arrays.asList(
        ImmutableMap.of("id", "alert-1",
            "level", "0",
            "serviceName", "service-1",
            "message", "test message"),
        ImmutableMap.of("id", "alert-2",
            "level", "1",
            "serviceName", "service-1",
            "message", "test info"),
        ImmutableMap.of("id", "alert-3",
            "level", "2",
            "serviceName", "service-1",
            "message", "test warning"),
        ImmutableMap.of("id", "alert-4",
            "level", "3",
            "serviceName", "service-2",
            "message", "test error"),
        ImmutableMap.of("id", "alert-5",
            "level", "4",
            "serviceName", "service-2",
            "message", "test debug")
    );

    public DataFetcher getAlertByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            String alertId = dataFetchingEnvironment.getArgument("id");
            return alerts
                .stream()
                .filter(alert -> alert.get("id").equals(alertId))
                .findFirst()
                .orElse(null);
        };
    }

    public DataFetcher getAlertsByLevelDataFetcher() {
        return dataFetchingEnvironment -> {
            String alertLevel = dataFetchingEnvironment.getArgument("level").toString();
            return alerts
                .stream()
                .filter(alert -> alert.get("level").equals(alertLevel))
                .toArray();
        };
    }

    public DataFetcher getAlertsByServiceNameDataFetcher() {
        return dataFetchingEnvironment -> {
            String alertServiceName = dataFetchingEnvironment.getArgument("serviceName");
            return alerts
                .stream()
                .filter(alert -> alert.get("serviceName").equals(alertServiceName))
                .toArray();
        };
    }
}
