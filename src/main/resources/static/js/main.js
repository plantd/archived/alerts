function networkBlip() {
    var $networking = $('.networking');
    if (!$networking.is(":visible")) {
        $networking.show(100, function () {
            var $that = $(this);
            setTimeout(function () {
                $that.hide();
            }, 500)
        });
    }
}

function subscribeToAlerts() {
    var exampleSocket = new WebSocket("ws://localhost:9001/alerts");

    networkBlip();

    exampleSocket.onopen = function () {
        networkBlip();
        console.log("web socket opened");

        var query = 'subscription AlertsSubscription { \n' +
            '    alerts(level: 1) {' +
            '       id\n' +
            '       serviceName\n' +
            '       message\n' +
            '     }' +
            '}';

        var graphqlMsg = {
            query: query,
            variables: {}
        };

        exampleSocket.send(JSON.stringify(graphqlMsg));
    };

    var ALERTS_UPDATES = {};

    exampleSocket.onmessage = function (event) {
        networkBlip();
        var data = JSON.parse(event.data);
        var msg = data.alerts;
        var alertId = msg.id;
        var alertList = ALERTS_UPDATES[alertId];

        if (alertList) {
            if (alertList.length > 7) {
                alertList.shift()
            }
            alertList.push(msg)
        } else {
            ALERTS_UPDATES[alertId] = [msg];
        }

        var htmlStr = '';
        Object.keys(ALERTS_UPDATES).forEach(function (alertId) {
            var alertList = ALERTS_UPDATES[alertId];

            htmlStr += '<div class="alertWrapper">';
            htmlStr += '<span class="alertId"> ' + alertId + ' </span>';

            alertList.forEach(function (alert) {
                var alertServiceName = alert['serviceName'];
                var alertMessage = alert['message'];
                htmlStr += '<span class="alertServiceName">' + alertServiceName + '</span>';
                htmlStr += '<span class="alertMessage">' + alertMessage + '</span>';
            });
            htmlStr += '</div>';
        });

        $('.alertList').html(htmlStr)
    };
}

window.addEventListener("load", subscribeToAlerts);
