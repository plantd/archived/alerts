# Plantd Alerts Service

[WIP] Still using mock data.

A service to receive alerts and notifications from `plantd` services, store
them, and publish them to clients using GraphQL subscriptions.

## Running

### Development

During development the server can be built and run locally using the command:

```sh
mvn spring-boot:run
```

### Packaging

To create a single `.jar` file that can be run elsewhere use the command:

```sh
mvn package
# run with
java -jar target/alerts-0.0.1-SNAPSHOT.jar
```

### Docker

The application can also be run in a container use Docker if the packaging
steps have been run.

```sh
docker build -t plantd-alerts .
docker run -it --rm --name alerts plantd-alerts
```

### Publishing

Docker builds are kept on GitLab using the repository specific container
registry. Publishing to this is done with these commands. This is only useful
to a maintainer with the appropriate permissions.

```sh
docker build -t registry.gitlab.com/plantd/alerts:v1 .
docker push registry.gitlab.com/plantd/alerts:v1
```
